Tfswitch
=========

Role that installs Tfswitch

Requirements
------------

None

Role Variables
--------------

Role that installs Tfswitch

Dependencies
------------

None

Example Playbook
----------------

Installing Tfswitch on targeted machine:

    - hosts: servers
      roles:
         - tfswitch

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
